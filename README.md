![Screenshot_2015-04-16-20-26-00.png](https://bitbucket.org/repo/k96r6b/images/3776050203-Screenshot_2015-04-16-20-26-00.png)


The only app you need for dealing with [the button](http://www.reddit.com/r/thebutton/) in your life.

Uses [emtes' button api](http://www.reddit.com/r/thebutton/comments/321jw1/button_stats_api_version_01/).

Requires appCompat_v7 on your android build path.