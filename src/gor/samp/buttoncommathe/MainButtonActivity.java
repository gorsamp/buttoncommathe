package gor.samp.buttoncommathe;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import android.support.v7.app.ActionBarActivity;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainButtonActivity extends ActionBarActivity {

	private final static String strongMessage = "You've stayed strong for ";
	private final static String strongSinceMessage = "You've stayed strong in total for ";

	ButtonTimerTask buttonTimerTask;
	MediaPlayer buttonDown, buttonUp;
	long strongStartTime, strongBirthTime;
	ImageButton theButton;
	TextView strongText, strongSinceText, strongTimerText;
	LinearLayout theBox, box1, box2, box3, box4, box5, box6;
	SimpleDateFormat sdf;
	Date strongBirthDate;
	int purpleCount, blueCount, greenCount, yellowCount, orangeCount, redCount; //, fallenSize;

	protected PowerManager.WakeLock wakeLock;

	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		initUI();
		sdf = new SimpleDateFormat("MM/dd/yyyy");
		strongStartTime = System.currentTimeMillis();
		try{
			strongBirthDate = sdf.parse("04/01/2015");
			strongBirthTime = strongBirthDate.getTime();
		} catch (ParseException e) {
			// whatever
		}
		if(bundle!=null){
			strongStartTime = bundle.getLong("start_time");
			// fallenSize = bundle.getInt("fallen_size");
			populatePressers(bundle.getInt("purple"), 
					bundle.getInt("blue"), 
					bundle.getInt("green"), 
					bundle.getInt("yellow"), 
					bundle.getInt("orange"), 
					bundle.getInt("red")
					);
		}

	}

	public void onResume(){
		if(buttonTimerTask==null){
			buttonTimerTask = new ButtonTimerTask();
			buttonTimerTask.execute();
		}
		final PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
		this.wakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");
		this.wakeLock.acquire();
		super.onResume();
	}

	public void onPause(){
		this.wakeLock.release();
		super.onPause();
	}

	protected void onSaveInstanceState(Bundle bundle){
		buttonTimerTask.cancel(true);
		bundle.putInt("purple", purpleCount);
		bundle.putInt("blue", blueCount);
		bundle.putInt("green", greenCount);
		bundle.putInt("yellow", yellowCount);
		bundle.putInt("orange", orangeCount);
		bundle.putInt("red", redCount);
		bundle.putLong("start_time", strongStartTime);
		super.onSaveInstanceState(bundle);
	}

	private void initUI(){

		if(getResources().getConfiguration().orientation==Configuration.ORIENTATION_PORTRAIT){
			setContentView(R.layout.activity_main_button);
		} else {
			setContentView(R.layout.activity_main_button_landscape);
		}

		strongText = (TextView)findViewById(R.id.button_comma_the_text);
		strongSinceText = (TextView)findViewById(R.id.button_comma_the_other_text);
		strongTimerText = (TextView)findViewById(R.id.button_comma_the_image_text);
		strongTimerText.setText("?");


		theButton = (ImageButton) findViewById(R.id.button_comma_the_image);
		theButton.setOnTouchListener(new ButtonTouchListener());

		theBox = (LinearLayout)findViewById(R.id.button_comma_the_scrollbox);		
		box1 = (LinearLayout)findViewById(R.id.scroll_box_1);
		box2 = (LinearLayout)findViewById(R.id.scroll_box_2);
		box3 = (LinearLayout)findViewById(R.id.scroll_box_3);
		box4 = (LinearLayout)findViewById(R.id.scroll_box_4);
		box5 = (LinearLayout)findViewById(R.id.scroll_box_5);
		box6 = (LinearLayout)findViewById(R.id.scroll_box_6);
	}

	class ButtonTouchListener implements OnTouchListener {

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			switch(event.getAction()){
			case MotionEvent.ACTION_DOWN:
				theButton.setImageResource(R.drawable.button_circle_dickbutt);
				buttonDown = MediaPlayer.create(MainButtonActivity.this, R.raw.button_in);
				buttonDown.setOnCompletionListener(new ButtonCompleteListener());
				buttonDown.start();
				break;
			case MotionEvent.ACTION_UP:
				theButton.setImageResource(R.drawable.button_circle);
				buttonUp = MediaPlayer.create(MainButtonActivity.this, R.raw.button_out);
				buttonUp.setOnCompletionListener(new ButtonCompleteListener());
				buttonUp.start();
				break;
			default:
				v.performClick();
				break;
			}
			return false;
		}
	}

	class ButtonCompleteListener implements OnCompletionListener {

		@Override
		public void onCompletion(MediaPlayer mp) {
			mp.reset();
			mp.release();
			mp=null;		
		}
	}

	public String toTime(long t) {
		double h = 0, m = 0, s = 0;
		s = (t * 0.001); // convert to seconds
		// count minutes
		m = Math.floor(s * 0.016666);
		// count hours
		while (60 < m) {
			h++;
			m -= 60; // subtract hours from minutes
		}
		// count seconds
		s -= (m * 60 + h * 3600); // subtract minutes and hours from seconds

		String hour = (int) h + ":";
		String minute = (int) m + ":";
		String second = (int) s + "";
		if (h < 1) {
			hour = "";
		}
		if (m < 9.99) {
			minute = "0"+(int)m+":";
		}
		if (s < 9.99) {
			second = "0" + (int) s;
		}
		return hour + minute + second;
	}

	private boolean measureSoon = false;
	private void populatePressers(int purple, int blue, int green, int yellow, int orange, int red){
		if(purple==0 
				&& blue==0
				&& green==0
				&& yellow==0
				&& orange==0
				&& red==0
				&& !measureSoon){
			return;
		}

		// clear view
		box1.removeAllViews();
		box2.removeAllViews();
		box3.removeAllViews();
		box4.removeAllViews();
		box5.removeAllViews();
		box6.removeAllViews();


		purpleCount += purple;
		blueCount += blue;
		greenCount += green;
		yellowCount += yellow;
		orangeCount += orange;
		redCount += red;

		int fallenHeight = box1.getMeasuredHeight();
		int fallenWidth = theBox.getMeasuredWidth();
		if(fallenHeight==0 || fallenWidth==0){
			measureSoon = true;
			return;
		} 

		int fallenSizePurple = Math.min(fallenHeight, purpleCount==0?fallenHeight+1:(int)(fallenWidth/purpleCount));
		int fallenSizeBlue = Math.min(fallenHeight, blueCount==0?fallenHeight+1:(int)(fallenWidth/blueCount));
		int fallenSizeGreen = Math.min(fallenHeight, greenCount==0?fallenHeight+1:(int)(fallenWidth/greenCount));
		int fallenSizeYellow = Math.min(fallenHeight, yellowCount==0?fallenHeight+1:(int)(fallenWidth/yellowCount));
		int fallenSizeOrange = Math.min(fallenHeight, orangeCount==0?fallenHeight+1:(int)(fallenWidth/orangeCount));		
		int fallenSizeRed = Math.min(fallenHeight, redCount==0?fallenHeight+1:(int)(fallenWidth/redCount));

		LinearLayout.LayoutParams lpp = new LinearLayout.LayoutParams(fallenSizePurple-8, fallenSizePurple-8);
		lpp.leftMargin = 4;
		lpp.gravity = Gravity.CENTER;
		LinearLayout.LayoutParams lpb = new LinearLayout.LayoutParams(fallenSizeBlue-8, fallenSizeBlue-8);
		lpb.leftMargin = 4;
		lpb.gravity = Gravity.CENTER;
		LinearLayout.LayoutParams lpg = new LinearLayout.LayoutParams(fallenSizeGreen-8, fallenSizeGreen-8);
		lpg.leftMargin = 4;
		lpg.gravity = Gravity.CENTER;
		LinearLayout.LayoutParams lpy = new LinearLayout.LayoutParams(fallenSizeYellow-8, fallenSizeYellow-8);
		lpy.leftMargin = 4;
		lpy.gravity = Gravity.CENTER;
		LinearLayout.LayoutParams lpo = new LinearLayout.LayoutParams(fallenSizeOrange-8, fallenSizeOrange-8);
		lpo.leftMargin = 4;
		lpo.gravity = Gravity.CENTER;
		LinearLayout.LayoutParams lpr = new LinearLayout.LayoutParams(fallenSizeRed-8, fallenSizeRed-8);
		lpr.leftMargin = 4;
		lpr.gravity = Gravity.CENTER;

		for(int i=0; i<purpleCount; i++){
			ImageView circleImage = new ImageView(this);
			circleImage.setImageResource(R.drawable.button_circle_purple);
			box1.addView(circleImage, lpp);
		}

		for(int i=0; i<blueCount; i++){
			ImageView circleImage = new ImageView(this);
			circleImage.setImageResource(R.drawable.button_circle_blue);
			box2.addView(circleImage, lpb);
		}

		for(int i=0; i<greenCount; i++){
			ImageView circleImage = new ImageView(this);
			circleImage.setImageResource(R.drawable.button_circle_green);
			box3.addView(circleImage, lpg);
		}

		for(int i=0; i<yellowCount; i++){
			ImageView circleImage = new ImageView(this);
			circleImage.setImageResource(R.drawable.button_circle_yellow);
			box4.addView(circleImage, lpy);
		}

		for(int i=0; i<orangeCount; i++){
			ImageView circleImage = new ImageView(this);
			circleImage.setImageResource(R.drawable.button_circle_orange);
			box5.addView(circleImage, lpo);
		}

		for(int i=0; i<redCount; i++){
			ImageView circleImage = new ImageView(this);
			circleImage.setImageResource(R.drawable.button_circle_red);
			box6.addView(circleImage, lpr);
		}
	}

	private String readConnection(HttpURLConnection urlConnection){
		String output = "";
		BufferedReader in = null;
		try {
			in = new BufferedReader(
					new InputStreamReader(
							urlConnection.getInputStream()));
			JsonParser jp = new JsonParser();
			JsonElement je = jp.parse(in);
			output = je.toString();
			in.close();
		} catch (IOException e) {
			Log.e("button","error reading", e);
		}

		return output;
	}

	class ButtonTimerTask extends AsyncTask<Void, String, Void>{

		private long flairTime = 15000;

		@TargetApi(Build.VERSION_CODES.HONEYCOMB)
		@Override
		protected void onProgressUpdate(String... strings){
			if(strings.length>1){
				if(strings[0].equalsIgnoreCase("tick")){
					// update strength text
					strongText.setText(strongMessage+toTime(System.currentTimeMillis()-strongStartTime));
					strongSinceText.setText(strongSinceMessage+toTime(System.currentTimeMillis()-strongBirthTime));	

					double diff = 0;
					int time = 0;  

					try {
						time = (new JSONObject(strings[1])).getInt("timer_value");
						diff = time*0.0166666;
					} catch (JSONException e) {
						Log.e("button", "error parsing tick json", e);
					}

					if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB){
						theButton.setScaleX((float)diff);
						theButton.setScaleY((float)diff);
					}
					strongTimerText.setText(time+"s");
				}
				if(strings[0].equalsIgnoreCase("flair")){
					try {
						JSONObject json = new JSONObject(strings[1]);
						populatePressers(json.getInt("purple"), json.getInt("blue"), json.getInt("green"), json.getInt("yellow"), json.getInt("orange"), json.getInt("red"));


					} catch (JSONException e) {
						Log.e("button", "error parsing flair json", e);
					}
				}
			}
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			HttpURLConnection urlConnection = null;
			try {
				long startTime = System.currentTimeMillis(), shortStartTime = System.currentTimeMillis();
				while(true && !this.isCancelled()){

					long thisOtherTime = System.currentTimeMillis()-shortStartTime;

					if(thisOtherTime>=333){
						URL url = new URL("http://button.mtsanderson.com/api/stats/ticks/latest");
						urlConnection = (HttpURLConnection) url.openConnection();
						publishProgress(new String[]{"tick", readConnection(urlConnection)});
						long timeDiff = System.currentTimeMillis()-shortStartTime;
						if(timeDiff>=666){
							shortStartTime = System.currentTimeMillis()-666;
						} else {
							shortStartTime = System.currentTimeMillis()-timeDiff;
						}
					}

					long thisTime = System.currentTimeMillis()-startTime;

					if(thisTime>=flairTime || measureSoon){
						int flairRequestCount = (int)Math.round(thisTime*0.001);
						if(flairRequestCount>0){
							URL url = new URL("http://button.mtsanderson.com/api/stats/flair/"+flairRequestCount);
							urlConnection = (HttpURLConnection) url.openConnection();
							publishProgress(new String[]{"flair", readConnection(urlConnection)});
							startTime = System.currentTimeMillis();
						}
					}
				}
			} catch(Exception e){
				Log.e("button", "error contacting santa mountain", e);
			}
			finally {
				if(urlConnection!=null){
					urlConnection.disconnect();
				}
			}
			return null;
		}
	}

}



